import React from 'react';
import wind from './assets/wind.png';
import humidity from './assets/humidity.png';

const Box = (weather) => {
    return (
        <div className="grid-margin stretch-card">
            <div className="card">
                <div className="card-body">
                    <div className="row grid-margin justify-content-center">
                        <div className="col-sm-6 mt-4 mt-lg-0">
                            <div className="px-4 py-4 card">
                                <div className="row">
                                    <div className="col-md-6 col-sm-12 col-xs-12 align-center">
                                        <h3 className="font-weight-bold text-dark">{weather.name}, {weather.sys.country}</h3>
                                        <h1 className="text-dark font-weight-bold">{Math.round(weather.main.temp)}°c</h1>
                                        <div className="d-lg-flex align-items-baseline mb-3">
                                            <p className="text-muted ml-3">{weather.weather[0].main}</p>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-sm-12 col-xs-12 temp-info-border align-center">
                                        <div className="position-relative">
                                            <img src={`https://openweathermap.org/img/wn/${weather.weather[0].icon}.png`} alt="weather" width="100" height="100" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row justify-content-center">
                        <div className="col-sm-6 mt-4 mt-lg-0">
                            <div className="bg-secondary text-white px-4 py-4 card">
                                <div className="row">
                                    <div className="col-md-6 col-sm-12 col-xs-12 pl-lg-5 align-center">
                                        <h2><img src={humidity} alt="humidity" /></h2>
                                        <p className="mb-0">{weather.main.humidity} %</p>
                                    </div>
                                    <div className="col-md-6 col-sm-12 col-xs-12 climate-info-border mt-lg-0 mt-2 align-center">
                                        <h2><img src={wind} alt="wind" /></h2>
                                        <p className="mb-0">{weather.wind.speed} m/s</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Box;