import React, { useState } from 'react';
import Box from './Box.js';

const api = {
  key: "XXXXXXXXXXXX",
  base: "https://api.openweathermap.org/data/2.5/"
}

function App() {
  const [query, setQuery] = useState('');
  const [weather, setWeather] = useState({});

  const search = evt => {
    if (evt.key === "Enter") {
      fetch(`${api.base}weather?q=${query}&units=metric&APPID=${api.key}`)
        .then(res => res.json())
        .then(result => {
          setWeather(result);
          setQuery('');
        });
    }
  }

  return (
    <div className='bg-primary'>
      <div className="container">
        <div className="grid-margin stretch-card">
          <div className="card">
            <div className="card-body">
              <div className="form-group">
                <input id="fullname" type="text" placeholder="Search by city name" className="form-control"
                  name="fullname" onChange={e => setQuery(e.target.value)}
                  value={query}
                  onKeyPress={search} required />
              </div>
            </div>
          </div>
        </div>

        {(Object.keys(weather).length) ? (
          <Box {...weather} />
        ) : ('')}
      </div>
    </div>
  );
}

export default App;